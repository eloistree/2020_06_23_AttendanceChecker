﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SavableSlider : MonoBehaviour, I_TextSavable
{
    public Slider m_linkedInputField;
    public string m_defaultText = "0";
    public bool m_withNotification = true;
    public string GetSavableDefaultText()
    {
        return m_defaultText;
    }

    public string GetSavableText()
    {
        return m_linkedInputField.value.ToString();
    }
    public void SetTextFromLoad(string text)
    {
        SetTextFromLoad(text, m_withNotification);
    }
    public void SetTextFromLoad(string text, bool withNotificaiton = true)
    {
        float value = 0;
        bool parsed = float.TryParse(text, out value);

        if (withNotificaiton) m_linkedInputField.value = value;
        else m_linkedInputField.SetValueWithoutNotify(value);
    }

    private void Reset()
    {
        m_linkedInputField = GetComponent<Slider>();
        DirectoryStorageLoadAndSave saver = GetComponent<DirectoryStorageLoadAndSave>();
        if (saver == null)
            gameObject.AddComponent<DirectoryStorageLoadAndSave>();
    }
}
