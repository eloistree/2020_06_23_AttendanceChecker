﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_DateAsYYYYMMDD : MonoBehaviour
{
    public int m_year;
    public int m_month;
    public int m_day;
   

    public void SetYear(string value) {
        float yyyy = 0;
        if(float.TryParse(value.Trim(), out yyyy)){
            m_year =(int) yyyy;
        }
    }
    public void SetYear(float value) { m_year = (int)value; }
    public void SetDay(float value) { m_day = (int)value; }
    public void SetMonth(float value) {  m_month = (int)value; }
}
