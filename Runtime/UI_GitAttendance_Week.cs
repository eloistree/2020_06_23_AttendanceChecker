﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GitAttendance_Week : MonoBehaviour
{
    public Text m_weekTitle;
    public UI_GitAttendance_MorningAfternoon m_monday;
    public UI_GitAttendance_MorningAfternoon m_tuesday;
    public UI_GitAttendance_MorningAfternoon m_wednesday;
    public UI_GitAttendance_MorningAfternoon m_thursday;
    public UI_GitAttendance_MorningAfternoon m_friday;
    public UI_GitAttendance_MorningAfternoon m_saturday;
    public UI_GitAttendance_MorningAfternoon m_sunday;
   
    public void SetWeekTitle(string text) {
        m_weekTitle.text = text;
    }
    public void HideAllDaysValue() {
        m_monday.Hide(true);
        m_tuesday.Hide(true);
        m_wednesday.Hide(true);
        m_thursday.Hide(true);
        m_friday.Hide(true);
        m_saturday.Hide(true);
        m_sunday.Hide(true);
    }
    public void SetDayCommit(DateTime time, bool morningCommit, bool afternoonCommit) {
        UI_GitAttendance_MorningAfternoon selected = null;
        switch (time.DayOfWeek)
        {
            case DayOfWeek.Monday:
                selected = m_monday;
                break;
            case DayOfWeek.Tuesday:
                selected = m_tuesday;
                break;
            case DayOfWeek.Wednesday:
                selected = m_wednesday;
                break;
            case DayOfWeek.Thursday:
                selected = m_thursday;
                break;
            case DayOfWeek.Friday:
                selected = m_friday;
                break;
            case DayOfWeek.Saturday:
                selected = m_saturday;
                break;
            case DayOfWeek.Sunday:
                selected = m_sunday;
                break;
            default:
                break;
        }
        if (selected != null) { 
            selected.Hide(false);
            selected.SetWith(morningCommit, afternoonCommit);
            selected.SetDateAsText( ""+time.Day);
        }



    }

    public enum DayType:int { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday }
}


