﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

//Look at pretty format: https://git-scm.com/docs/pretty-formats
public class DraftExpeirmentAttendance : MonoBehaviour
{
    public string m_gitFullPathRepo = "C:\\...";
    public string[] m_gitFullPathRepositories = new string[0];
    //"git log --after=\"2013-11-12 00:00\" --before=\"2013-11-12 23:59\"";
    public string m_cmd = "git log --after=\"2013-11-12 00:00\" --before=\"2013-11-12 23:59\" ";
    
    public int m_max = 10000;
    public List<WindowCMDCallback> m_resultOfLastCmd;
    public GitDateFormat m_from, m_to;
    public List<LogCommitReceived> m_previousCommits = new List<LogCommitReceived>();

    public List<LogCommitReceived> GetFoundCommits()
    {
        return m_previousCommits;
    }


    public void SetRepositoryAbsolutePath(string path)
    {
        m_gitFullPathRepo = path;
    }

    public void SetRepositoriesAbsolutePath(string [] path)
    {
        m_gitFullPathRepositories = path;
    }


    //https://github.com/EloiStree/2020_06_23_GroupAttendanceChecker/issues/1
    //https://github.com/EloiStree/2020_06_23_GroupAttendanceChecker/issues/2

    [ContextMenu("Load Info from git")]
    public void LoadInformationFromGit() {
       
        QuickGit.LoadCommitsFromDateToDate(m_gitFullPathRepositories, m_from, m_to,  out m_previousCommits, out m_resultOfLastCmd, m_max);

    }
}



