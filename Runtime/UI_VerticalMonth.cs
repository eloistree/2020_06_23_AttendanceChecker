﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_VerticalMonth : MonoBehaviour
{
    public string m_value = "January";
    public Text m_monthText;

    public void SetMonthName(string monthText) {
        if (monthText == null) return;
        string t = "";
        foreach (char c in monthText.ToCharArray())
        {
            t += c + "\n";
        }
        m_monthText.text = t;
    }
    private void OnValidate()
    {
        SetMonthName(m_value);
    }
}
