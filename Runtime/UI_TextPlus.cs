﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_TextPlus : MonoBehaviour
{

    public Text m_text;
    public void SetTextWith(int value) { m_text.text = value.ToString(); }
    public void SetTextWith(float value) { m_text.text = value.ToString(); }
    private void Reset()
    {
        m_text = GetComponent<Text>();
    }
}
