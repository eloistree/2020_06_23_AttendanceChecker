﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GitAttendance_MorningAfternoon : MonoBehaviour
{
    public Text m_dateText;
    public Image m_morning;
    public Image m_afternoon;
    public Color m_hasCommit = Color.green;
    public Color m_noCommit= Color.grey;


    public void SetDateAsText(string date)
    {
        if (m_dateText)
            m_dateText.text = date;
    }
    public void SetWith(bool hasCommitMorning, bool hasCommitAfternoon)
    {
        if (m_morning)
            m_morning.color = hasCommitMorning ? m_hasCommit : m_noCommit;
        if (m_afternoon)
            m_afternoon.color = hasCommitAfternoon ? m_hasCommit : m_noCommit;
    }

    public void Hide(bool value)
    {
        if (m_morning)
            m_morning.gameObject.SetActive(!value);
        if (m_afternoon)
            m_afternoon.gameObject.SetActive(!value);
        if(m_dateText)
        m_dateText.gameObject.SetActive(!value);
    }
}
