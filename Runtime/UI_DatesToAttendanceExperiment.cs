﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UI_DatesToAttendanceExperiment : MonoBehaviour
{
    public string m_mustContainAtLeast = "";
    public UI_DateAsYYYYMMDD m_from, m_to;
    public DraftExpeirmentAttendance m_dateExperiment;
    public GameObject m_uiDate;
    public GameObject m_uiWeek;
    public GameObject m_uiMonth;
    public Transform m_whereToInstanciateIt;
    public double m_days;
    public bool m_startAtFirstCommit;
    public bool m_endAtFirstCommit;

    public void SetInformationToLookFor(string text) {
        m_mustContainAtLeast = text;
    }

    public void Refresh() {

        DateTime from, to;
        KillChildren(m_whereToInstanciateIt);
        SetWith(m_dateExperiment.m_from, m_from);
        SetWith(m_dateExperiment.m_to, m_to);
        from = m_dateExperiment.m_from.GetAsDateTime();
        to = m_dateExperiment.m_to.GetAsDateTime();
        m_dateExperiment.LoadInformationFromGit();

        List<LogCommitReceived> previousCommits = m_dateExperiment.GetFoundCommits();
        if (m_mustContainAtLeast.Length > 0) {
            previousCommits = previousCommits.Where(k =>
            k.m_author.ToLower().IndexOf(m_mustContainAtLeast.ToLower()) >= 0 ||
            k.m_userMail.ToLower().IndexOf(m_mustContainAtLeast.ToLower()) >= 0).ToList();
        }
        previousCommits = previousCommits.OrderBy(k => k.GetDate()).ToList();
        if (previousCommits.Count == 0) return;

        if (m_startAtFirstCommit)
            from = previousCommits[0].GetDate();
        if (m_endAtFirstCommit)
            to = previousCommits[previousCommits.Count-1].GetDate();


        DateTime dateIndex = from;
        dateIndex.AddDays(-1f);
        //DIRTY LIKE HELL BUT I NEED TO START SOMEWHERE
        m_days = (to - from).TotalDays;
        UI_GitAttendance_Week currentWeek=null;
        GameObject created=null;
        bool firstCommit = true;
        
        while (dateIndex < to.AddDays(1))
        {
            if (firstCommit || (dateIndex.Month==1  && dateIndex.Day== 1))
            {
                created = GameObject.Instantiate(m_uiMonth, m_whereToInstanciateIt);
                UI_VerticalMonth monthScript = created.GetComponent<UI_VerticalMonth>();
                if (monthScript != null)
                {
                    monthScript.SetMonthName(dateIndex.Year.ToString());
                }

            }
            if (dateIndex.Day == 1)
            {
                created = GameObject.Instantiate(m_uiMonth, m_whereToInstanciateIt);
                UI_VerticalMonth monthScript = created.GetComponent<UI_VerticalMonth>();
                if (monthScript != null)
                {
                    monthScript.SetMonthName(dateIndex.ToString("MMMM"));
                }
            }
            if (dateIndex.DayOfWeek==DayOfWeek.Monday || dateIndex.Day == 1 || currentWeek==null) {
                created = GameObject.Instantiate(m_uiWeek, m_whereToInstanciateIt);
                currentWeek = created.GetComponent<UI_GitAttendance_Week>();
                if (currentWeek != null)
                {
                    currentWeek.SetWeekTitle("");// Should be the week of the months
                    currentWeek.HideAllDaysValue();
                }
            }


            DateTime morning = new DateTime(dateIndex.Year, dateIndex.Month, dateIndex.Day, 0, 0, 0, 0, 0);
            DateTime midDay = morning.AddHours(12);
            DateTime night = morning.AddHours(24);
            List<LogCommitReceived> morningCommit = previousCommits.Where(
                k => k.GetDate()> morning && k.GetDate() < midDay).ToList();
            List<LogCommitReceived> nightCommit = previousCommits.Where(
                k => k.GetDate() > midDay && k.GetDate() < night).ToList();

            currentWeek.SetDayCommit(dateIndex, morningCommit.Count > 0, nightCommit.Count > 0);

                //created = GameObject.Instantiate(m_uiDate, m_whereToInstanciateIt);
                //UI_GitAttendance_MorningAfternoon day = created.GetComponent<UI_GitAttendance_MorningAfternoon>();
                //if (day != null)
                //{
                //    day.SetDateAsText(dateIndex.Month+"/"+ dateIndex.Day);
                //    day.SetWith(morningCommit.Count > 0, nightCommit.Count > 0);
                //}
            
            dateIndex = dateIndex.AddDays(1);
            firstCommit = false;
        }
        
    }

    private int GetIndexOf(DayOfWeek dayOfWeek)
    {
        switch (dayOfWeek)
        {
            case DayOfWeek.Monday: return 0;
            case DayOfWeek.Tuesday: return 1;
            case DayOfWeek.Wednesday: return 2;
            case DayOfWeek.Thursday: return 3;
            case DayOfWeek.Friday: return 4;
            case DayOfWeek.Saturday: return 5;
            case DayOfWeek.Sunday:return 6;
            default:
                return 0;
        }
    }

    private void SetWith(GitDateFormat gitDate, UI_DateAsYYYYMMDD uiDate)
    {
        gitDate.year = uiDate.m_year;
        gitDate.month = uiDate.m_month;
        gitDate.day =  uiDate.m_day;
    }

    private void KillChildren(Transform whereToInstanciateIt)
    {
        //GameObject parent = whereToInstanciateIt.gameObject;
        //GameObject [] toKill =  whereToInstanciateIt.gameObject.GetComponentsInChildren<GameObject>(true);
        //for (int i = 0; i < toKill.Length; i++)
        //{
        //    if (toKill[i].gameObject != parent) {
        //        Destroy(toKill[i].gameObject);
        //    }

        //}
        //Source: https://answers.unity.com/questions/611850/destroy-all-children-of-object.html
        int childs = whereToInstanciateIt.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.DestroyImmediate(whereToInstanciateIt.GetChild(i).gameObject);
        }
    }
    
}
